package Tema5PT;

import java.time.LocalDateTime;
import java.util.Date;

public class MonitoredData {
	
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	
	
	public MonitoredData(Date startTime,Date endTime, String activityLabel) {
		
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
		
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	@Override
	public String toString() {
		return this.startTime+" "+this.endTime+" "+activityLabel ;
	}
	
	

}
