package Tema5PT;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImplementMethods {
	
	private List<MonitoredData> activityList = new ArrayList<MonitoredData>();
	private String fileName = "Activity.txt";
	private List<String[]> list =new ArrayList<>();
	private Map<Object, Long> sum =new HashMap<Object, Long>();
	private Map<Object, Long> sum2 =new HashMap<Object, Long>();
	private Map<Object, Long> task4 =new HashMap<Object, Long>();
	private Map<Object, Date> task =new HashMap<Object, Date>();
	
	
	public void readFile() {
			
		SimpleDateFormat dt = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss"); 
		try (Stream<String> stream = Files.lines(Paths.get(fileName)))  {  //create a stream that will get a path to a text file where we will read all lines from it as a stream

			list= stream.map(line->line.split("\t\t")) //file contains 3 columns that are split between them with 2 tabs, use this method to split the file
			    		.collect(Collectors.toList()); //the data will be added to the list
			     		      
		} catch (IOException e) {
			e.printStackTrace();
		}
      
	  for(String[] s:list) {
		  
		 Date startTime=null;
		try {
			startTime = dt.parse(s[0]);  //first column of the file contains the startTime format
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		 Date endTime=null;
		try {
			endTime = dt.parse(s[1]);   //second column of the file contains the endTime format
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		  activityList.add(new MonitoredData(startTime,endTime,s[2]));  //s[2] will represent the activity, all the data is added to the list
	  }
	  
	  //System.out.println(activityList);
	}

	
	public void task1() {
		
		
		/*System.out.println("  "+(int)activityList.stream()
		             .map(a->a.getStartTime().getDate()) //get all the starttime and all the dates from the activityList
		             .distinct() 						 //see how many of them are distinct
		             .count());		*/				 //count
		             		
	}
	
	
	public void task2() {
		   sum = activityList.stream().collect(
	                Collectors.groupingBy(item->item.getActivityLabel(), Collectors.counting()));//we will group all the data from the activityList by the activityLabel and then 
		           																				 //the counting() function will count how many activities of each are in the list
		   String filename="Task2.txt";
			try {
				FileWriter fw = new FileWriter(filename,true);
				sum.entrySet()
				   .stream()
				   .forEach(a->{
					try {
						fw.write(a.toString());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
				fw.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}    
		
		//System.out.println(sum);
		
	}
	
	public void task4() {
		
		
		task4= activityList.stream().collect(
				 Collectors.groupingBy(item->item.getActivityLabel(), Collectors.summingLong(a->a.getEndTime().getTime()-a.getStartTime().getTime())));
 //the activityList will be grouped now by the activityLabel, the function will return a Collector that produced the sum of the activities which fullfill the condition	
 //the condition is the difference between the endtime and the starttime because we want to have the total duration of each activity 
		task4.entrySet()
		     .stream()
		     .filter(a->a.getValue()>36000000) //now we will take into consideration only the activities with a total duration higher than 10 hours
		     .forEach(a->task.put(a.getKey(), new Date(a.getValue()))); //forEach element in the array, we will place it in another array after the filter was succeeded
		 System.out.println(task);  									//first the name and then the time
		 																//the new list with our final results will be printed
		   String filename="Task4.txt";
					try {
						FileWriter fw = new FileWriter(filename,true);
						task.entrySet()
						   .stream()
						   .forEach(a->{
							try {
								fw.write(a.toString());
								fw.write("\n");
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});
						fw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}    		
	}
	
	
	
	public void task5() {
		
		
		List<String> list=new ArrayList<>();
		
		
		  sum2 = activityList.stream()
				    .filter(a->(a.getEndTime().getTime()-a.getStartTime().getTime())<=300000)
				    .collect(
	                Collectors.groupingBy(item->item.getActivityLabel(), Collectors.counting()));
		  //System.out.println(sum2);
		  sum2.entrySet()
		     .stream()
		     .filter(a->a.getValue()>90/100*sum.get(a.getKey()))
		     .forEach(a-> list.add((String)a.getKey()));
		 // System.out.println(list);
	}
}
